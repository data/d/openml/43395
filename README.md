# OpenML dataset: Disaster-Tweets

https://www.openml.org/d/43395

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The file contains over 11,000 tweets associated with disaster keywords like crash, quarantine, and bush fires as well as the location and keyword itself. The data structure was inherited from Disasters on social media
The tweets were collected on Jan 14th, 2020.
Some of the topics people were tweeting:

The eruption of Taal Volcano in Batangas, Philippines
Coronavirus
Bushfires in Australia
Iran downing of the airplane flight PS752

Disclaimer: The dataset contains text that may be considered profane, vulgar, or offensive.
Inspiration
The intention was to enrich the already available data for this topic with newly collected and manually classified tweets.
The initial source Disasters on social media which is used in Real or Not? NLP with Disaster Tweets competition on Kaggle.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43395) of an [OpenML dataset](https://www.openml.org/d/43395). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43395/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43395/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43395/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

